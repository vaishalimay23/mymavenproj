FROM tomcat
COPY demo.war /usr/webapps
EXPOSE 8080
CMD ["catalina", "start"]
